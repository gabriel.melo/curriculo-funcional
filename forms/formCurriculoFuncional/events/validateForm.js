function validateForm(form) {

    var msg = "";

    if (form.getValue("inputExperienciaFuncao") == "") {
        msg += "Campo 'Experiência na Função' não foi preenchido.";
    }

    if (form.getValue("inputConhecimentoSolucao") == "") {
        msg += "Campo 'Conhecimento nas Soluções' TOTVS não foi preenchido.";
    }

    var indexesFormacao = form.getChildrenIndexes('formacaoPaiFilho'); 
    var indexesProcessos = form.getChildrenIndexes('processosPaiFilho');  

    if (indexesFormacao.length < 1) {
        msg += "Nenhuma formação cadastrada."
    } else {
        for (var i = 0; i < indexesFormacao.length; i++) {
            if (form.getValue("inputCursoPaiFilho___" + indexesFormacao[i]) == "") {
                msg += "Curso não foi informado";
            }
            if (form.getValue("inputInstituicaoPaiFilho___" + indexesFormacao[i] == "")) {
                msg += "Instituição não foi informada";
            }
            if (form.getValue("inputStatusPaiFilho___" + indexesFormacao[i] == "")) {
                msg += "Status não foi informado";
            }
        }
    }

    if (indexesProcessos.length < 1) {
        msg += "Nenhuma experiência cadastrada."
    } else {
        for (var i = 0; i < indexesProcessos.length; i++) {
            if (form.getValue("inputNomeProjetoPaiFilho___" + indexesProcessos[i]) == "") {
                msg += "Nome do Projeto não foi informado";
            }
            if (form.getValue("inputEmpresaPaiFilho___" + indexesProcessos[i] == "")) {
                msg += "Empresa não foi informada";
            }
            if (form.getValue("inputFuncaoPaiFilho___" + indexesProcessos[i] == "")) {
                msg += "Função no projeto não foi informado";
            }
            if (form.getValue("inputPeriodoPaiFilho___" + indexesProcessos[i] == "")) {
                msg += "Período não foi informado";
            }
        }
    }

    if (msg != "") {
        throw msg;
    }

}